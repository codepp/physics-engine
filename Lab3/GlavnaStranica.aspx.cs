﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lab3
{
    public partial class GlavnaStranica : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string[] listaNaBoi = Enum.GetNames(typeof(System.Drawing.KnownColor));
                ddlPozadina.DataSource = listaNaBoi;
                ddlFont.DataSource = listaNaBoi;

                lblKorisnici.Text = Najava._korisnici.ToString();
                lblKorisnik.Text = Session["korisnik"].ToString();

                if (Request.Cookies["korisnik"] != null)
                {
                    ddlPozadina.SelectedValue = Request.Cookies["korisnik"].Values["bg"].ToString();
                    ddlFont.SelectedValue = Request.Cookies["korisnik"].Values["fg"].ToString();
                    lblPoraka.Text = Request.Cookies["korisnik"].Values["ll"].ToString();

                    pnlPanela1.ForeColor = Color.FromName(Request.Cookies["korisnik"].Values["fg"].ToString());
                    pnlPanela1.BackColor = Color.FromName(Request.Cookies["korisnik"].Values["bg"].ToString());
                }
                else
                {
                    lblPoraka.Text = Session["korisnik"].ToString() + ", ова е твојата прва посета. Избери боја на фонт и позадина и потоа кликни зачувај.";
                }
            }

            this.DataBind();
        }

        protected void ddlPozadina_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlPanela1.BackColor = Color.FromName(ddlPozadina.SelectedItem.Text);
        }

        protected void ddlFont_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlPanela1.ForeColor = Color.FromName(ddlFont.SelectedItem.Text);
        }

        protected void btnZacuvaj_Click(object sender, EventArgs e)
        {
            var cookie = new HttpCookie("korisnik");
            if (Request.Cookies["korisnik"] != null)
            {
                cookie = Request.Cookies["korisnik"];
            }
            cookie.Values["bg"] = ddlPozadina.SelectedItem.Text;
            cookie.Values["fg"] = ddlFont.SelectedItem.Text;
            cookie.Values["ll"] = DateTime.Now.ToString();

            cookie.Expires = DateTime.Now.AddMinutes(5);

            lblPoraka.Text = "Креирано е колаче";

            Response.Cookies.Add(cookie);
        }

        protected void btnOdjava_Click(object sender, EventArgs e)
        {
            Interlocked.Decrement(ref Najava._korisnici);
            Response.Redirect(string.Format("/Odjava.aspx?q={0}", Session.SessionID));
        }
    }
}