﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lab3
{
    public partial class Najava : System.Web.UI.Page
    {
        public static int _korisnici = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnPodnesi_Click(object sender, EventArgs e)
        {
            if (txtLozinka.Text == "mp")
            {
                Interlocked.Increment(ref _korisnici);
                Session["korisnik"] = txtKorisnik.Text;
                Response.Redirect("/GlavnaStranica.aspx");
            }
            else
            {
                int obidi = 0;
                int.TryParse(lblObidi.Text, out obidi);
                lblObidi.Text = (obidi + 1).ToString();

                if (obidi == 3)
                {
                    lblObidi.Text = "Го надминавте максималниот број на обиди за најава! Не ви е дозволено да се најавите";
                    btnPodnesi.Enabled = false;
                }
            }
        }
    }
}