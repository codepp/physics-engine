﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GlavnaStranica.aspx.cs" Inherits="Lab3.GlavnaStranica" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Panel ID="pnlPanela1" runat="server">
                Најавен корсник:
                <asp:Label ID="lblKorisnik" runat="server"></asp:Label><br />
                Фкупно најавени корисници:
                <asp:Label ID="lblKorisnici" runat="server"></asp:Label><br />
                <asp:DropDownList ID="ddlPozadina" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPozadina_SelectedIndexChanged"></asp:DropDownList><br />
                <asp:DropDownList ID="ddlFont" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFont_SelectedIndexChanged"></asp:DropDownList><br />
                <asp:Label ID="lblPoraka" runat="server"></asp:Label><br />
                <asp:Button ID="btnZacuvaj" runat="server" Text="Зачувај" OnClick="btnZacuvaj_Click"></asp:Button>
                <asp:Button ID="btnOdjava" runat="server" Text="Одјава" OnClick="btnOdjava_Click"></asp:Button>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
