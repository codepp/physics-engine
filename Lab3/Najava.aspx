﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Najava.aspx.cs" Inherits="Lab3.Najava" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <asp:Label ID="lblKorisnik" runat="server" Text="Корисничко име"></asp:Label>
                <asp:TextBox ID="txtKorisnik" runat="server"></asp:TextBox>
            </div>
            <div>
                <asp:Label ID="lblLozinka" runat="server" Text="Лозинка"></asp:Label>
                <asp:TextBox ID="txtLozinka" runat="server" TextMode="Password"></asp:TextBox>
            </div>
            <div>
                <asp:Button ID="btnPodnesi" runat="server" Text="Поднеси" OnClick="btnPodnesi_Click"></asp:Button>
            </div>
            <div>
                Број на обиди <br/>
                <asp:Label ID="lblObidi" runat="server" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </form>
</body>
</html>
