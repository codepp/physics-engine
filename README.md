Welcome!

First inspired by the author's excursions into linear algebra, this project represents a video game engine,
written into hand-tuned C++. The engine has been loosely modeled after Unity3D, and is meant to have the
following modules and features:

    1. CoreMath: An implementation of the standard objects in linear algebra, such as: vectors, matrices, 
       and quaterions. This module was written with performance in number crunching in mind.

    2. Physics: An implementation of a simple physics engine, supporting commmon operations, such as: 
       transforms, translations, kinematics, ray casting, ray tracing and collision detection.

    3. Artificial Intelligence: An implementation of a standard artificial intelligence engine, providing
       common AI concepts, such as: finite state machnines, decision trees, pathfinding, etc.
       
At this moment, development is still focused on the first module.