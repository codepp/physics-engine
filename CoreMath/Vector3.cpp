#include "Vector3.h"
#include <math.h>

using namespace CoreMath;

/* Constructors. */
Vector3::Vector3(float i, float j, float k)
{
	this->i = i;
	this->j = j;
	this->k = k;
}

Vector3::Vector3(const Vector3& toCopy)
{
	this->i = toCopy.i;
	this->j = toCopy.j;
	this->k = toCopy.k;
}

Vector3::Vector3(const Vector2& fromVector2)
{
	this->i = fromVector2.i;
	this->j = fromVector2.j;
	this->k = 0;
}

/* Operators. */
Vector3 Vector3::operator=(const Vector3& toAssign)
{
	this->i = toAssign.i;
	this->j = toAssign.j;
	this->k = toAssign.k;
	return *this;
}

bool Vector3::operator==(const Vector3& toCompare) const
{
	return (this->i == toCompare.i) && (this->j == toCompare.j) && (this->k == toCompare.k); 
}

bool Vector3::operator!=(const Vector3& toCompare) const
{
	return !((this->i == toCompare.i) && (this->j == toCompare.j) && (this->k == toCompare.k)); 
}

Vector3 Vector3::operator*(const float multiplier) const
{
	return Vector3(this->i*multiplier, this->j * multiplier, this->k * multiplier);
}

Vector3 Vector3::operator+(const Vector3& toAdd) const
{
	return Vector3(this->i + toAdd.i, this->j + toAdd.j, this->k + toAdd.k);
}

Vector3 Vector3::operator-(const Vector3& toSubtract) const
{
	return Vector3(this->i - toSubtract.i, this->j - toSubtract.j, this->k - toSubtract.k);
}

Vector3 Vector3::operator-() const
{
	return *this * -1;
}

/* Common vector operations. */ 
float Vector3::Magnitude(void) const
{
	return sqrt(this->SqrMagnitude());
}

float Vector3::SqrMagnitude(void) const
{
	return (this->i * this->i) + (this->j * this->j) + (this->k * this->k);
}

void Vector3::Normalize(void)
{
	float vectMagnitude = this->Magnitude();
	this->i /= vectMagnitude;
	this->j /= vectMagnitude;
	this->k /= vectMagnitude;
}

Vector3 Vector3::Normalize(const Vector3& toNormalize)
{
	float vectMagnitude = toNormalize.Magnitude();
	return Vector3(toNormalize.i / vectMagnitude, toNormalize.j / vectMagnitude, toNormalize.k / vectMagnitude);
}

float Vector3::ScalarProduct(const Vector3& v1, const Vector3& v2)
{
	float scalarProduct = 0;
	scalarProduct += (v1.i * v2.i);
	scalarProduct += (v1.j * v2.j);
	scalarProduct += (v1.k * v2.k);

	return scalarProduct;
}

Vector3 Vector3::VectorProduct(const Vector3& u, const Vector3& v)
{
	float i = (i = u.j - v.k) - (v.j * u.k);
	float j = -(u.i * v.k) + (v.i * u.k);
	float k = (u.i  * v.j) - (u.j * v.i);

	return Vector3(i, j, k);
}

float Vector3::AngleBetween(Vector3 v1, Vector3 v2)
{
	v1.Normalize();
	v2.Normalize();
	float product = Vector3::ScalarProduct(v1, v2);
	return acos(product);
}

