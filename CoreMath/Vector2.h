#pragma once

namespace CoreMath
{

	class Vector2
	{
	public:
		/* Data members -- vector dimensions. */
		float i, j;

		/* Constructors. */
		Vector2(float, float);
		Vector2(const Vector2&);

		/* Operators. */
		Vector2 operator=(const Vector2&);
		bool operator==(const Vector2&) const;
		bool operator!=(const Vector2&) const;
		Vector2 operator+(const Vector2&) const;
		Vector2 operator-(const Vector2&) const;
		Vector2 operator*(const float) const;
		Vector2 operator-() const;

		/*Standard vector operations. */
		float Magnitude(void) const;
		float SqrMagnitude(void) const;
		void Normalize(void);

		/* Static member functions. Used for internal operations. */

		static Vector2 Normalize(const Vector2&);
		static float ScalarProduct(const Vector2&, const Vector2&);
		static float AngleBetween(Vector2, Vector2);
	};
}
