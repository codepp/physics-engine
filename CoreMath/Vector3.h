#pragma once
#include "Vector2.h"

namespace CoreMath
{
	class Vector3
	{
	public:
		/* Data members -- vector dimensions. */
		float i, j, k;

		/* Constructors. */
		Vector3(float, float, float);
		Vector3(const Vector3&);
		Vector3(const Vector2&);

		/* Operators. */
		Vector3 operator=(const Vector3&);
		bool operator==(const Vector3&) const;
		bool operator!=(const Vector3&) const;
		Vector3 operator+(const Vector3&) const;
		Vector3 operator-(const Vector3&) const;
		Vector3 operator*(const float) const;
		Vector3 operator-() const;

		/*Standard vector operations. */
		void Normalize(void);
		float Magnitude(void) const;
		float SqrMagnitude(void) const;

		static Vector3 Normalize(const Vector3&);
		static float ScalarProduct(const Vector3&, const Vector3&);
		static Vector3 VectorProduct(const Vector3&, const Vector3&);
		static float AngleBetween(Vector3, Vector3);
	};
}

