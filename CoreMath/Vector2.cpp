#include "Vector2.h"
#include <math.h>

using namespace CoreMath;
/* Constructors. */
Vector2::Vector2(float i, float j)
{
	this->i = i;
	this->j = j;
}

Vector2::Vector2(const Vector2& toCopy)
{
	this->i = toCopy.i;
	this->j = toCopy.j;
}

/* Operators. */
bool Vector2::operator==(const Vector2& toCompare) const
{
	return (this->i == toCompare.i) && (this->j == toCompare.j);
}

bool Vector2::operator!=(const Vector2& toCompare) const
{
	return (this->i != toCompare.i && this->j != toCompare.j);
}

Vector2 Vector2::operator=(const Vector2& toAssign)
{
	this->i = toAssign.i;
	this->j = toAssign.j;
	return *this;
}

Vector2 Vector2::operator+(const Vector2& toAdd) const
{
	return Vector2(this->i + toAdd.i, this->j + toAdd.j);
}

Vector2 Vector2::operator-(const Vector2& toSubtract) const
{
	return Vector2(this->i - toSubtract.i, this->j - toSubtract.j);
}

Vector2 Vector2::operator*(const float multiplier) const
{
	return Vector2(this->i * multiplier, this->j * multiplier);
}

Vector2 Vector2::operator-() const
{
	return *this * -1;
}

/* Standard vector operations. */
void Vector2::Normalize(void)
{
	const float vectMagnitude = this->Magnitude();
	this->i /= vectMagnitude;
	this->j /= vectMagnitude;
}

Vector2 Vector2::Normalize(const Vector2& toNormalize)
{
	float vectMagnitude = toNormalize.Magnitude();
	return Vector2(toNormalize.i / vectMagnitude, toNormalize.j / vectMagnitude);
}

float Vector2::Magnitude() const
{
	return sqrt(this -> SqrMagnitude());
}

float Vector2::SqrMagnitude() const
{
	return (this->i * this->i) + (this->j * this->j);
}

float Vector2 :: ScalarProduct(const Vector2& v1, const Vector2& v2)
{
	float scalarProduct = 0;
	scalarProduct += (v1.i * v2.i);
	scalarProduct += (v1.j * v2.j);

	return scalarProduct;
}

float Vector2::AngleBetween(Vector2 v1, Vector2 v2)
{
	/* 
		Optimization trick!
		Factors out the magnitudes.
	*/

	v1.Normalize();
	v2.Normalize();
	float product = Vector2::ScalarProduct(v1, v2);
	return acos(product);
}
